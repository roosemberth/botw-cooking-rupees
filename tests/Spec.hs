{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

-- BotwCP
import BotwCP.Parser

-- QuickCheck
import Test.QuickCheck

-- base
import Control.Monad (guard)
import Data.Char
import Data.Either
import Numeric.Natural (Natural)

-- containers
import qualified Data.Map as Map
import qualified Data.Set as Set

-- parsec
import Text.Parsec

-- text
import Data.Text (Text)
import qualified Data.Text as T

-- transformers
import Control.Monad.Trans.Except (except)

-- quickcheck-instances
import Test.QuickCheck.Instances

strip :: String -> String
strip = T.unpack . T.strip . T.pack

isValidIngrName :: String -> Bool
isValidIngrName name = not $ null (strip name) || '\n' `elem` name || '"' `elem` name

prop_ingredient_withmul :: String -> Natural -> Property
prop_ingredient_withmul name amount =
  isValidIngrName name ==> Right (strip name, amount) === pRes
  where pRes = parse ingredient "(test)" (name ++ "x" ++ show amount)

prop_ingredient_nomul :: String -> Property
prop_ingredient_nomul name =
  isValidIngrName name && all (not . isNumber) name ==>
    Right (strip name, 1) === pRes
  where pRes = parse ingredient "(test)" name

recipeNamesGen :: Gen [Text]
recipeNamesGen = do
  firstName <- suchThat (arbitrary :: Gen Text) (isValidIngrName . T.unpack)
  names <- suchThat (arbitrary :: Gen [Text]) (all (isValidIngrName . T.unpack))
  return (firstName:names)

recipeIngrGen :: Gen ([Text], [Natural])
recipeIngrGen = do
  names <- recipeNamesGen
  nRepeated <- choose (1, length names - 1)
  multipliers <- sequence $ replicate nRepeated (arbitrary :: Gen Natural)
  return (names, multipliers)

prop_toRecipe :: Property
prop_toRecipe = forAll recipeIngrGen test_toRecipe
  where
    test_toRecipe (names, amounts) = except (Right eRecipe) === toRecipe
      [ "Test recipe"
      , T.unlines ingredients
      , "100"
      ]
      where
        ingredients = zipWith (<>) names (map (("x"<>) . T.pack . show) amounts ++ repeat "")
        eIngredients = zipWith (,) (T.strip <$> names) (amounts ++ repeat 1)
        eRecipe = Recipe "Test recipe" eIngredients 100

prop_ingredientsFile :: Property
prop_ingredientsFile = forAll recipeNamesGen $ \names ->
  Right (Set.fromList $ mkExpIngr <$> names) === parse ingredientsFile "" (unlines $ mkIngrLine <$> names)
  where
    mkExpIngr name = Ingredient (T.strip name) 10
    mkIngrLine = T.unpack . toQuotedCsv . (\name -> [name, "10"])

toQuotedCsv :: [Text] -> Text
toQuotedCsv [] = ""
toQuotedCsv (a:as) = quoted a <> "," <> toQuotedCsv as
  where quoted elem = "\"" <> elem <> "\""

hasNoDuplicates ls = hasNoDuplicates' [] ls
  where hasNoDuplicates' _ [] = True
        hasNoDuplicates' seen (a:as) = notElem a seen && hasNoDuplicates' (a:seen) as

prop_inventoryFile :: Property
prop_inventoryFile = forAll recipeNamesGen $ \names ->
  hasNoDuplicates names ==>
  Right (Map.fromList $ mkExpRecord <$> names) === parse inventoryFile "" (unlines $ mkInvLine <$> names)
  where
    mkExpRecord name = (T.strip name, 10)
    mkInvLine = T.unpack . toQuotedCsv . (\name -> [name, "10"])

-- needed on GHC 7.8 and later; without it, quickCheckAll will not be able to find any of the properties.
return []
main = $quickCheckAll
