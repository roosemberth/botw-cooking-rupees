{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

-- BotwCP
import BotwCP.Parser (Ingredients, Inventory, Recipe)
import qualified BotwCP.Parser as P

-- base
import Data.Functor ((<&>))
import Data.Maybe
import Numeric.Natural
import System.Exit (exitFailure)
import System.IO (hPutStrLn, stderr)

-- cmdargs
import System.Console.CmdArgs

-- containers
import qualified Data.Set as Set
import qualified Data.Map as Map

-- filepath
import System.FilePath.Posix

-- mtl
import Control.Monad.Except

-- parsec
import Text.ParserCombinators.Parsec (GenParser, ParseError, parse)

-- text
import Data.Text (Text)
import qualified Data.Text as T

data ArgsDesc = ArgsDesc
  { ingredients :: FilePath
  , dishes :: FilePath
  , inventory :: FilePath
  } deriving (Data, Show, Typeable)

argDesc = ArgsDesc
  { ingredients = def &= argPos 0 &= typ "INGREDIENTS"
  , dishes = def &= argPos 1 &= typ "DISHES"
  , inventory = def &= argPos 2 &= typ "INVENTORY"
  } &= details
  [ "DISHES: Path to the CSV containing dishes, ingredients and their price."
  , "INGREDIENTS: Path to the CSV containing ingredient names and their price."
  , "INVENTORY: Path to the CSV containing the currently hold items and the amount."
  ]

emptyToNothing :: [a] -> Maybe [a]
emptyToNothing [] = Nothing
emptyToNothing ls = Just ls

-- | If a recipe has a problem, this function returns a description of it.
recipeProblem :: Ingredients -> Recipe -> Maybe Text
recipeProblem validIngrs r@P.Recipe{P.name, P.ingredients} =
    T.unlines <$> (header:) <$> emptyToNothing (toErrMsg <$> filter isUnknown ingrNames)
  where
    header = "Recipe '" <> name <> "' has the following problems:"
    toErrMsg name = "Ingredient '" <> name <> "' does not exist."
    isUnknown ingredient = ingredient `notElem` validNames
    validNames = map (P.name :: P.Ingredient -> Text) (Set.toList validIngrs)
    ingrNames = map fst ingredients

-- | If an inventory has a problem, this function returns a description of it.
inventoryProblem :: Ingredients -> Inventory -> Maybe Text
inventoryProblem validIngrs inventory =
    T.unlines <$> emptyToNothing (toErrMsg <$> (filter isUnknown $ Map.keys inventory))
  where
    toErrMsg name = "Ingredient '" <> name <> "' does not exist."
    isUnknown ingredient = ingredient `notElem` validNames
    validNames = map (P.name :: P.Ingredient -> Text) (Set.toList validIngrs)

parseFile :: GenParser Char () a -> FilePath -> ExceptT ParseError IO a
parseFile parser file = ExceptT $ parse parser (takeFileName file) <$> readFile file

whenJust :: Monad m => m (Maybe a) -> (a -> m ()) -> m ()
whenJust condition callback = condition >>= maybe (pure ()) callback

die message = hPutStrLn stderr ("Error: " ++ message ++ ".") >> exitFailure

main :: IO ()
main = do
  let runExceptT' ex = runExceptT ex >>= either (fail . show) pure
  args <- cmdArgs argDesc
  recipes <- runExceptT' $ parseFile P.cookbook (dishes args)
  ingredients <- runExceptT' $ parseFile P.ingredientsFile (ingredients args)
  inventory <- runExceptT' $ parseFile P.inventoryFile (inventory args)

  sequence_ [whenJust (pure $ recipeProblem ingredients r) (die . T.unpack) | r <- recipes]
  whenJust (pure $ inventoryProblem ingredients inventory) (die . T.unpack)
  putStrLn "Recipes and Inventory correctly validated against Ingredients."
