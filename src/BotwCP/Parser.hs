{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module BotwCP.Parser
  ( Ingredient(..)
  , Ingredients
  , Inventory
  , Recipe(..)
  , cookbook
  , ingredient
  , ingredientsFile
  , inventoryFile
  , toRecipe
  ) where

-- base
import Control.Monad (void)
import Control.Monad.Fail (fail)
import Data.Char
import Data.Either
import Data.Maybe
import Numeric.Natural (Natural)
import Prelude hiding (fail)
import Text.Read (readEither)

-- containers
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Map as Map

-- mtl
import Control.Monad.Except (Except, runExcept, throwError, withExceptT)

-- parsec
import Text.ParserCombinators.Parsec
import Text.Parsec.Error (mergeError)

-- text
import Data.Text (Text)
import qualified Data.Text as T

-- transformers
import Control.Monad.Trans.Except (except)

data Ingredient = Ingredient
  { name :: Text
  , price :: Natural
  } deriving (Eq, Show)

-- Delegate comparison to the ingredient name.
instance Ord Ingredient where
  compare (Ingredient a _) (Ingredient b _) = compare a b

type Ingredients = Set Ingredient

type Inventory = Map Text Natural

data Recipe = Recipe
  { name :: Text
  , ingredients :: [(Text, Natural)]
  , price :: Natural
  } deriving (Eq, Show)

csvFile :: GenParser Char st [[Text]]
csvFile = endBy line eol

eol :: GenParser Char st Char
eol = char '\n'

line :: GenParser Char st [Text]
line = sepBy cell (char ',')

cell :: GenParser Char st Text
cell = quotedCell <|> T.pack <$> many (noneOf "\",\n")

quotedCell :: GenParser Char st Text
quotedCell = between quote quote $ T.pack <$> many (noneOf "\"")
  where quote = char '"'

textToPrice :: Text -> Either String Natural
textToPrice = readEither . T.unpack

ingredientsFile :: GenParser Char st Ingredients
ingredientsFile = Set.fromList <$> endBy ingredientLine eol
  where ingredientLine = toIngredient =<< line
        toIngredient :: [Text] -> GenParser Char st Ingredient
        toIngredient (name:value:_) =
          Ingredient (T.strip name) <$> either fail return (textToPrice value)
        toIngredient _ = fail "Ingredient file line: Not enough values"

inventoryFile :: GenParser Char st Inventory
inventoryFile = Map.fromList <$> endBy inventoryLine eol
  where inventoryLine = toInv =<< line
        toInv :: [Text] -> GenParser Char st (Text, Natural)
        toInv (name:amount:_) =
          (\p -> (T.strip name, p)) <$> either fail return (textToPrice amount)
        toInv _ = fail "Inventory file line: Not enough values."

mapFst :: (a -> b) -> (a, c) -> (b, c)
mapFst fn (a, b) = (fn a, b)

-- | Ingredient name and amount
ingredient :: GenParser Char st (String, Natural)
ingredient = do
  let end = eof <|> lookAhead (void eol) <|> (space >> end)
      multiplier = do skipMany space
                      char 'x'
                      digits <- many1 digit
                      end
                      return $ read digits
  skipMany space
  word <- do
    start <- noneOf "\n" -- Names may start with 'x'
    rest <- many (satisfy $ not . (\c -> isSpace c || c == 'x'))
    return (start:rest)

  mul <- optionMaybe $ try multiplier <|> try (end >> return 1)
  spaces <- lookAhead $ many space

  if isJust mul
  then return (word, fromJust mul)
  else ingredient >>= \(rName, rMul) -> return (word ++ spaces ++ rName, rMul)

ingredientList :: GenParser Char st [(Text, Natural)]
ingredientList = sepEndBy (mapFst T.pack <$> ingredient) eol

foldLeft :: (a -> a -> a) -> [Either a b] -> Either a [b]
foldLeft op eithers = if null errs then Right res else Left $ foldl1 op errs
  where (errs, res) = partitionEithers eithers

toRecipe :: [Text] -> Except String Recipe
toRecipe (name:ingredientsTxt:price:_) = mkRecipe pIngredients
  where pIngredients :: Either ParseError [(Text, Natural)]
        pIngredients = parse ingredientList pname str
          where pname = "ingredient " ++ T.unpack name
                str = T.unpack ingredientsTxt
        mkRecipe :: Either ParseError [(Text, Natural)] -> Except String Recipe
        mkRecipe (Right ingredients) = Recipe name ingredients <$> except' (textToPrice price)
          where except' = withExceptT (const failMsg) . except
                failMsg = "Failed to parse price for ingredient '" ++ T.unpack name ++ "'"
        mkRecipe (Left err) = throwError $ show err
toRecipe args = throwError $ "Could not parse recipe " ++ show args

cookbook :: GenParser Char st [Recipe]
cookbook = do
  let mergeError' a b = a ++ "\n" ++ b
  recipes <- foldLeft mergeError' <$> fmap (runExcept . toRecipe) <$> csvFile
  either fail return recipes
